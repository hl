﻿#region License Information
/* HeuristicLab
 * Copyright (C) 2002-2011 Heuristic and Evolutionary Algorithms Laboratory (HEAL)
 *
 * This file is part of HeuristicLab.
 *
 * HeuristicLab is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HeuristicLab is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HeuristicLab. If not, see <http://www.gnu.org/licenses/>.
 */
#endregion

namespace HeuristicLab.Analysis.Views {
  partial class DataTableVisualPropertiesControl {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing) {
      if (disposing && (components != null)) {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent() {
      this.components = new System.ComponentModel.Container();
      this.label1 = new System.Windows.Forms.Label();
      this.yAxisPrimaryTitleTextBox = new System.Windows.Forms.TextBox();
      this.yAxisSecondaryTitleTextBox = new System.Windows.Forms.TextBox();
      this.label4 = new System.Windows.Forms.Label();
      this.xAxisPrimaryGroupBox = new System.Windows.Forms.GroupBox();
      this.label12 = new System.Windows.Forms.Label();
      this.label9 = new System.Windows.Forms.Label();
      this.xAxisPrimaryTitleTextBox = new System.Windows.Forms.TextBox();
      this.label11 = new System.Windows.Forms.Label();
      this.xAxisPrimaryMaximumPanel = new System.Windows.Forms.Panel();
      this.xAxisPrimaryMaximumFixedTextBox = new System.Windows.Forms.TextBox();
      this.xAxisPrimaryMaximumFixedRadioButton = new System.Windows.Forms.RadioButton();
      this.xAxisPrimaryMaximumAutoRadioButton = new System.Windows.Forms.RadioButton();
      this.xAxisPrimaryMinimumPanel = new System.Windows.Forms.Panel();
      this.xAxisPrimaryMinimumFixedTextBox = new System.Windows.Forms.TextBox();
      this.xAxisPrimaryMinimumFixedRadioButton = new System.Windows.Forms.RadioButton();
      this.xAxisPrimaryMinimumAutoRadioButton = new System.Windows.Forms.RadioButton();
      this.xAxisSecondaryGroupBox = new System.Windows.Forms.GroupBox();
      this.label10 = new System.Windows.Forms.Label();
      this.xAxisSecondaryTitleTextBox = new System.Windows.Forms.TextBox();
      this.label5 = new System.Windows.Forms.Label();
      this.label6 = new System.Windows.Forms.Label();
      this.xAxisSecondaryMaximumPanel = new System.Windows.Forms.Panel();
      this.xAxisSecondaryMaximumFixedTextBox = new System.Windows.Forms.TextBox();
      this.xAxisSecondaryMaximumFixedRadioButton = new System.Windows.Forms.RadioButton();
      this.xAxisSecondaryMaximumAutoRadioButton = new System.Windows.Forms.RadioButton();
      this.xAxisSecondaryMinimumPanel = new System.Windows.Forms.Panel();
      this.xAxisSecondaryMinimumFixedTextBox = new System.Windows.Forms.TextBox();
      this.xAxisSecondaryMinimumFixedRadioButton = new System.Windows.Forms.RadioButton();
      this.xAxisSecondaryMinimumAutoRadioButton = new System.Windows.Forms.RadioButton();
      this.axisTabControl = new System.Windows.Forms.TabControl();
      this.mainTabPage = new System.Windows.Forms.TabPage();
      this.axisFontLabel = new System.Windows.Forms.Label();
      this.titleFontLabel = new System.Windows.Forms.Label();
      this.axisFontButton = new System.Windows.Forms.Button();
      this.label3 = new System.Windows.Forms.Label();
      this.titleFontButton = new System.Windows.Forms.Button();
      this.label2 = new System.Windows.Forms.Label();
      this.xAxisTabPage = new System.Windows.Forms.TabPage();
      this.yAxisTabPage = new System.Windows.Forms.TabPage();
      this.yAxisSecondaryGroupBox = new System.Windows.Forms.GroupBox();
      this.label13 = new System.Windows.Forms.Label();
      this.label14 = new System.Windows.Forms.Label();
      this.yAxisSecondaryMaximumPanel = new System.Windows.Forms.Panel();
      this.yAxisSecondaryMaximumFixedTextBox = new System.Windows.Forms.TextBox();
      this.yAxisSecondaryMaximumFixedRadioButton = new System.Windows.Forms.RadioButton();
      this.yAxisSecondaryMaximumAutoRadioButton = new System.Windows.Forms.RadioButton();
      this.yAxisSecondaryMinimumPanel = new System.Windows.Forms.Panel();
      this.yAxisSecondaryMinimumFixedTextBox = new System.Windows.Forms.TextBox();
      this.yAxisSecondaryMinimumFixedRadioButton = new System.Windows.Forms.RadioButton();
      this.yAxisSecondaryMinimumAutoRadioButton = new System.Windows.Forms.RadioButton();
      this.yAxisPrimaryGroupBox = new System.Windows.Forms.GroupBox();
      this.label7 = new System.Windows.Forms.Label();
      this.label8 = new System.Windows.Forms.Label();
      this.yAxisPrimaryMaximumPanel = new System.Windows.Forms.Panel();
      this.yAxisPrimaryMaximumFixedTextBox = new System.Windows.Forms.TextBox();
      this.yAxisPrimaryMaximumFixedRadioButton = new System.Windows.Forms.RadioButton();
      this.yAxisPrimaryMaximumAutoRadioButton = new System.Windows.Forms.RadioButton();
      this.yAxisPrimaryMinimumPanel = new System.Windows.Forms.Panel();
      this.yAxisPrimaryMinimumFixedTextBox = new System.Windows.Forms.TextBox();
      this.yAxisPrimaryMinimumFixedRadioButton = new System.Windows.Forms.RadioButton();
      this.yAxisPrimaryMinimumAutoRadioButton = new System.Windows.Forms.RadioButton();
      this.errorProvider = new System.Windows.Forms.ErrorProvider(this.components);
      this.titleFontDialog = new System.Windows.Forms.FontDialog();
      this.axisFontDialog = new System.Windows.Forms.FontDialog();
      this.xAxisPrimaryGroupBox.SuspendLayout();
      this.xAxisPrimaryMaximumPanel.SuspendLayout();
      this.xAxisPrimaryMinimumPanel.SuspendLayout();
      this.xAxisSecondaryGroupBox.SuspendLayout();
      this.xAxisSecondaryMaximumPanel.SuspendLayout();
      this.xAxisSecondaryMinimumPanel.SuspendLayout();
      this.axisTabControl.SuspendLayout();
      this.mainTabPage.SuspendLayout();
      this.xAxisTabPage.SuspendLayout();
      this.yAxisTabPage.SuspendLayout();
      this.yAxisSecondaryGroupBox.SuspendLayout();
      this.yAxisSecondaryMaximumPanel.SuspendLayout();
      this.yAxisSecondaryMinimumPanel.SuspendLayout();
      this.yAxisPrimaryGroupBox.SuspendLayout();
      this.yAxisPrimaryMaximumPanel.SuspendLayout();
      this.yAxisPrimaryMinimumPanel.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
      this.SuspendLayout();
      // 
      // label1
      // 
      this.label1.AutoSize = true;
      this.label1.Location = new System.Drawing.Point(4, 26);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(30, 13);
      this.label1.TabIndex = 0;
      this.label1.Text = "Title:";
      // 
      // yAxisPrimaryTitleTextBox
      // 
      this.yAxisPrimaryTitleTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                  | System.Windows.Forms.AnchorStyles.Right)));
      this.yAxisPrimaryTitleTextBox.Location = new System.Drawing.Point(63, 23);
      this.yAxisPrimaryTitleTextBox.Name = "yAxisPrimaryTitleTextBox";
      this.yAxisPrimaryTitleTextBox.Size = new System.Drawing.Size(221, 20);
      this.yAxisPrimaryTitleTextBox.TabIndex = 1;
      this.yAxisPrimaryTitleTextBox.Validated += new System.EventHandler(this.yPrimaryTitleTextBox_Validated);
      // 
      // yAxisSecondaryTitleTextBox
      // 
      this.yAxisSecondaryTitleTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                  | System.Windows.Forms.AnchorStyles.Right)));
      this.yAxisSecondaryTitleTextBox.Location = new System.Drawing.Point(63, 23);
      this.yAxisSecondaryTitleTextBox.Name = "yAxisSecondaryTitleTextBox";
      this.yAxisSecondaryTitleTextBox.Size = new System.Drawing.Size(243, 20);
      this.yAxisSecondaryTitleTextBox.TabIndex = 1;
      this.yAxisSecondaryTitleTextBox.Validated += new System.EventHandler(this.ySecondaryTitleTextBox_Validated);
      // 
      // label4
      // 
      this.label4.AutoSize = true;
      this.label4.Location = new System.Drawing.Point(4, 26);
      this.label4.Name = "label4";
      this.label4.Size = new System.Drawing.Size(30, 13);
      this.label4.TabIndex = 0;
      this.label4.Text = "Title:";
      // 
      // xAxisPrimaryGroupBox
      // 
      this.xAxisPrimaryGroupBox.Controls.Add(this.label12);
      this.xAxisPrimaryGroupBox.Controls.Add(this.label9);
      this.xAxisPrimaryGroupBox.Controls.Add(this.xAxisPrimaryTitleTextBox);
      this.xAxisPrimaryGroupBox.Controls.Add(this.label11);
      this.xAxisPrimaryGroupBox.Controls.Add(this.xAxisPrimaryMaximumPanel);
      this.xAxisPrimaryGroupBox.Controls.Add(this.xAxisPrimaryMinimumPanel);
      this.xAxisPrimaryGroupBox.Location = new System.Drawing.Point(6, 6);
      this.xAxisPrimaryGroupBox.Name = "xAxisPrimaryGroupBox";
      this.xAxisPrimaryGroupBox.Size = new System.Drawing.Size(316, 107);
      this.xAxisPrimaryGroupBox.TabIndex = 0;
      this.xAxisPrimaryGroupBox.TabStop = false;
      this.xAxisPrimaryGroupBox.Text = "Primary Axis";
      // 
      // label12
      // 
      this.label12.AutoSize = true;
      this.label12.Location = new System.Drawing.Point(4, 77);
      this.label12.Name = "label12";
      this.label12.Size = new System.Drawing.Size(54, 13);
      this.label12.TabIndex = 4;
      this.label12.Text = "Maximum:";
      // 
      // label9
      // 
      this.label9.AutoSize = true;
      this.label9.Location = new System.Drawing.Point(4, 26);
      this.label9.Name = "label9";
      this.label9.Size = new System.Drawing.Size(30, 13);
      this.label9.TabIndex = 0;
      this.label9.Text = "Title:";
      // 
      // xAxisPrimaryTitleTextBox
      // 
      this.xAxisPrimaryTitleTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                  | System.Windows.Forms.AnchorStyles.Right)));
      this.xAxisPrimaryTitleTextBox.Location = new System.Drawing.Point(63, 23);
      this.xAxisPrimaryTitleTextBox.Name = "xAxisPrimaryTitleTextBox";
      this.xAxisPrimaryTitleTextBox.Size = new System.Drawing.Size(221, 20);
      this.xAxisPrimaryTitleTextBox.TabIndex = 1;
      this.xAxisPrimaryTitleTextBox.Validated += new System.EventHandler(this.xPrimaryTitleTextBox_Validated);
      // 
      // label11
      // 
      this.label11.AutoSize = true;
      this.label11.Location = new System.Drawing.Point(4, 53);
      this.label11.Name = "label11";
      this.label11.Size = new System.Drawing.Size(51, 13);
      this.label11.TabIndex = 2;
      this.label11.Text = "Minimum:";
      // 
      // xAxisPrimaryMaximumPanel
      // 
      this.xAxisPrimaryMaximumPanel.Controls.Add(this.xAxisPrimaryMaximumFixedTextBox);
      this.xAxisPrimaryMaximumPanel.Controls.Add(this.xAxisPrimaryMaximumFixedRadioButton);
      this.xAxisPrimaryMaximumPanel.Controls.Add(this.xAxisPrimaryMaximumAutoRadioButton);
      this.xAxisPrimaryMaximumPanel.Location = new System.Drawing.Point(63, 71);
      this.xAxisPrimaryMaximumPanel.Name = "xAxisPrimaryMaximumPanel";
      this.xAxisPrimaryMaximumPanel.Size = new System.Drawing.Size(245, 25);
      this.xAxisPrimaryMaximumPanel.TabIndex = 5;
      // 
      // xAxisPrimaryMaximumFixedTextBox
      // 
      this.xAxisPrimaryMaximumFixedTextBox.Location = new System.Drawing.Point(126, 2);
      this.xAxisPrimaryMaximumFixedTextBox.Name = "xAxisPrimaryMaximumFixedTextBox";
      this.xAxisPrimaryMaximumFixedTextBox.Size = new System.Drawing.Size(95, 20);
      this.xAxisPrimaryMaximumFixedTextBox.TabIndex = 2;
      this.xAxisPrimaryMaximumFixedTextBox.Validating += new System.ComponentModel.CancelEventHandler(this.xAxisPrimaryMaximumFixedTextBox_Validating);
      // 
      // xAxisPrimaryMaximumFixedRadioButton
      // 
      this.xAxisPrimaryMaximumFixedRadioButton.AutoSize = true;
      this.xAxisPrimaryMaximumFixedRadioButton.Location = new System.Drawing.Point(70, 4);
      this.xAxisPrimaryMaximumFixedRadioButton.Name = "xAxisPrimaryMaximumFixedRadioButton";
      this.xAxisPrimaryMaximumFixedRadioButton.Size = new System.Drawing.Size(50, 17);
      this.xAxisPrimaryMaximumFixedRadioButton.TabIndex = 1;
      this.xAxisPrimaryMaximumFixedRadioButton.TabStop = true;
      this.xAxisPrimaryMaximumFixedRadioButton.Text = "Fixed";
      this.xAxisPrimaryMaximumFixedRadioButton.UseVisualStyleBackColor = true;
      this.xAxisPrimaryMaximumFixedRadioButton.CheckedChanged += new System.EventHandler(this.xAxisPrimaryMaximumRadioButton_CheckedChanged);
      // 
      // xAxisPrimaryMaximumAutoRadioButton
      // 
      this.xAxisPrimaryMaximumAutoRadioButton.AutoSize = true;
      this.xAxisPrimaryMaximumAutoRadioButton.Location = new System.Drawing.Point(4, 4);
      this.xAxisPrimaryMaximumAutoRadioButton.Name = "xAxisPrimaryMaximumAutoRadioButton";
      this.xAxisPrimaryMaximumAutoRadioButton.Size = new System.Drawing.Size(47, 17);
      this.xAxisPrimaryMaximumAutoRadioButton.TabIndex = 0;
      this.xAxisPrimaryMaximumAutoRadioButton.TabStop = true;
      this.xAxisPrimaryMaximumAutoRadioButton.Text = "Auto";
      this.xAxisPrimaryMaximumAutoRadioButton.UseVisualStyleBackColor = true;
      this.xAxisPrimaryMaximumAutoRadioButton.CheckedChanged += new System.EventHandler(this.xAxisPrimaryMaximumRadioButton_CheckedChanged);
      // 
      // xAxisPrimaryMinimumPanel
      // 
      this.xAxisPrimaryMinimumPanel.Controls.Add(this.xAxisPrimaryMinimumFixedTextBox);
      this.xAxisPrimaryMinimumPanel.Controls.Add(this.xAxisPrimaryMinimumFixedRadioButton);
      this.xAxisPrimaryMinimumPanel.Controls.Add(this.xAxisPrimaryMinimumAutoRadioButton);
      this.xAxisPrimaryMinimumPanel.Location = new System.Drawing.Point(63, 47);
      this.xAxisPrimaryMinimumPanel.Name = "xAxisPrimaryMinimumPanel";
      this.xAxisPrimaryMinimumPanel.Size = new System.Drawing.Size(245, 25);
      this.xAxisPrimaryMinimumPanel.TabIndex = 3;
      // 
      // xAxisPrimaryMinimumFixedTextBox
      // 
      this.xAxisPrimaryMinimumFixedTextBox.Location = new System.Drawing.Point(126, 2);
      this.xAxisPrimaryMinimumFixedTextBox.Name = "xAxisPrimaryMinimumFixedTextBox";
      this.xAxisPrimaryMinimumFixedTextBox.Size = new System.Drawing.Size(95, 20);
      this.xAxisPrimaryMinimumFixedTextBox.TabIndex = 2;
      this.xAxisPrimaryMinimumFixedTextBox.Validating += new System.ComponentModel.CancelEventHandler(this.xAxisPrimaryMinimumFixedTextBox_Validating);
      // 
      // xAxisPrimaryMinimumFixedRadioButton
      // 
      this.xAxisPrimaryMinimumFixedRadioButton.AutoSize = true;
      this.xAxisPrimaryMinimumFixedRadioButton.Location = new System.Drawing.Point(70, 3);
      this.xAxisPrimaryMinimumFixedRadioButton.Name = "xAxisPrimaryMinimumFixedRadioButton";
      this.xAxisPrimaryMinimumFixedRadioButton.Size = new System.Drawing.Size(50, 17);
      this.xAxisPrimaryMinimumFixedRadioButton.TabIndex = 1;
      this.xAxisPrimaryMinimumFixedRadioButton.TabStop = true;
      this.xAxisPrimaryMinimumFixedRadioButton.Text = "Fixed";
      this.xAxisPrimaryMinimumFixedRadioButton.UseVisualStyleBackColor = true;
      this.xAxisPrimaryMinimumFixedRadioButton.CheckedChanged += new System.EventHandler(this.xAxisPrimaryMinimumRadioButton_CheckedChanged);
      // 
      // xAxisPrimaryMinimumAutoRadioButton
      // 
      this.xAxisPrimaryMinimumAutoRadioButton.AutoSize = true;
      this.xAxisPrimaryMinimumAutoRadioButton.Location = new System.Drawing.Point(4, 3);
      this.xAxisPrimaryMinimumAutoRadioButton.Name = "xAxisPrimaryMinimumAutoRadioButton";
      this.xAxisPrimaryMinimumAutoRadioButton.Size = new System.Drawing.Size(47, 17);
      this.xAxisPrimaryMinimumAutoRadioButton.TabIndex = 0;
      this.xAxisPrimaryMinimumAutoRadioButton.TabStop = true;
      this.xAxisPrimaryMinimumAutoRadioButton.Text = "Auto";
      this.xAxisPrimaryMinimumAutoRadioButton.UseVisualStyleBackColor = true;
      this.xAxisPrimaryMinimumAutoRadioButton.CheckedChanged += new System.EventHandler(this.xAxisPrimaryMinimumRadioButton_CheckedChanged);
      // 
      // xAxisSecondaryGroupBox
      // 
      this.xAxisSecondaryGroupBox.Controls.Add(this.label10);
      this.xAxisSecondaryGroupBox.Controls.Add(this.xAxisSecondaryTitleTextBox);
      this.xAxisSecondaryGroupBox.Controls.Add(this.label5);
      this.xAxisSecondaryGroupBox.Controls.Add(this.label6);
      this.xAxisSecondaryGroupBox.Controls.Add(this.xAxisSecondaryMaximumPanel);
      this.xAxisSecondaryGroupBox.Controls.Add(this.xAxisSecondaryMinimumPanel);
      this.xAxisSecondaryGroupBox.Location = new System.Drawing.Point(6, 119);
      this.xAxisSecondaryGroupBox.Name = "xAxisSecondaryGroupBox";
      this.xAxisSecondaryGroupBox.Size = new System.Drawing.Size(316, 106);
      this.xAxisSecondaryGroupBox.TabIndex = 1;
      this.xAxisSecondaryGroupBox.TabStop = false;
      this.xAxisSecondaryGroupBox.Text = "Secondary Axis";
      // 
      // label10
      // 
      this.label10.AutoSize = true;
      this.label10.Location = new System.Drawing.Point(4, 26);
      this.label10.Name = "label10";
      this.label10.Size = new System.Drawing.Size(30, 13);
      this.label10.TabIndex = 0;
      this.label10.Text = "Title:";
      // 
      // xAxisSecondaryTitleTextBox
      // 
      this.xAxisSecondaryTitleTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                  | System.Windows.Forms.AnchorStyles.Right)));
      this.xAxisSecondaryTitleTextBox.Location = new System.Drawing.Point(63, 23);
      this.xAxisSecondaryTitleTextBox.Name = "xAxisSecondaryTitleTextBox";
      this.xAxisSecondaryTitleTextBox.Size = new System.Drawing.Size(221, 20);
      this.xAxisSecondaryTitleTextBox.TabIndex = 1;
      this.xAxisSecondaryTitleTextBox.Validated += new System.EventHandler(this.xSecondaryTitleTextBox_Validated);
      // 
      // label5
      // 
      this.label5.AutoSize = true;
      this.label5.Location = new System.Drawing.Point(4, 77);
      this.label5.Name = "label5";
      this.label5.Size = new System.Drawing.Size(54, 13);
      this.label5.TabIndex = 4;
      this.label5.Text = "Maximum:";
      // 
      // label6
      // 
      this.label6.AutoSize = true;
      this.label6.Location = new System.Drawing.Point(4, 53);
      this.label6.Name = "label6";
      this.label6.Size = new System.Drawing.Size(51, 13);
      this.label6.TabIndex = 2;
      this.label6.Text = "Minimum:";
      // 
      // xAxisSecondaryMaximumPanel
      // 
      this.xAxisSecondaryMaximumPanel.Controls.Add(this.xAxisSecondaryMaximumFixedTextBox);
      this.xAxisSecondaryMaximumPanel.Controls.Add(this.xAxisSecondaryMaximumFixedRadioButton);
      this.xAxisSecondaryMaximumPanel.Controls.Add(this.xAxisSecondaryMaximumAutoRadioButton);
      this.xAxisSecondaryMaximumPanel.Location = new System.Drawing.Point(63, 71);
      this.xAxisSecondaryMaximumPanel.Name = "xAxisSecondaryMaximumPanel";
      this.xAxisSecondaryMaximumPanel.Size = new System.Drawing.Size(245, 25);
      this.xAxisSecondaryMaximumPanel.TabIndex = 5;
      // 
      // xAxisSecondaryMaximumFixedTextBox
      // 
      this.xAxisSecondaryMaximumFixedTextBox.Location = new System.Drawing.Point(126, 2);
      this.xAxisSecondaryMaximumFixedTextBox.Name = "xAxisSecondaryMaximumFixedTextBox";
      this.xAxisSecondaryMaximumFixedTextBox.Size = new System.Drawing.Size(95, 20);
      this.xAxisSecondaryMaximumFixedTextBox.TabIndex = 2;
      this.xAxisSecondaryMaximumFixedTextBox.Validating += new System.ComponentModel.CancelEventHandler(this.xAxisSecondaryMaximumFixedTextBox_Validating);
      // 
      // xAxisSecondaryMaximumFixedRadioButton
      // 
      this.xAxisSecondaryMaximumFixedRadioButton.AutoSize = true;
      this.xAxisSecondaryMaximumFixedRadioButton.Location = new System.Drawing.Point(70, 4);
      this.xAxisSecondaryMaximumFixedRadioButton.Name = "xAxisSecondaryMaximumFixedRadioButton";
      this.xAxisSecondaryMaximumFixedRadioButton.Size = new System.Drawing.Size(50, 17);
      this.xAxisSecondaryMaximumFixedRadioButton.TabIndex = 1;
      this.xAxisSecondaryMaximumFixedRadioButton.TabStop = true;
      this.xAxisSecondaryMaximumFixedRadioButton.Text = "Fixed";
      this.xAxisSecondaryMaximumFixedRadioButton.UseVisualStyleBackColor = true;
      this.xAxisSecondaryMaximumFixedRadioButton.CheckedChanged += new System.EventHandler(this.xAxisSecondaryMaximumRadioButton_CheckedChanged);
      // 
      // xAxisSecondaryMaximumAutoRadioButton
      // 
      this.xAxisSecondaryMaximumAutoRadioButton.AutoSize = true;
      this.xAxisSecondaryMaximumAutoRadioButton.Location = new System.Drawing.Point(4, 4);
      this.xAxisSecondaryMaximumAutoRadioButton.Name = "xAxisSecondaryMaximumAutoRadioButton";
      this.xAxisSecondaryMaximumAutoRadioButton.Size = new System.Drawing.Size(47, 17);
      this.xAxisSecondaryMaximumAutoRadioButton.TabIndex = 0;
      this.xAxisSecondaryMaximumAutoRadioButton.TabStop = true;
      this.xAxisSecondaryMaximumAutoRadioButton.Text = "Auto";
      this.xAxisSecondaryMaximumAutoRadioButton.UseVisualStyleBackColor = true;
      this.xAxisSecondaryMaximumAutoRadioButton.CheckedChanged += new System.EventHandler(this.xAxisSecondaryMaximumRadioButton_CheckedChanged);
      // 
      // xAxisSecondaryMinimumPanel
      // 
      this.xAxisSecondaryMinimumPanel.Controls.Add(this.xAxisSecondaryMinimumFixedTextBox);
      this.xAxisSecondaryMinimumPanel.Controls.Add(this.xAxisSecondaryMinimumFixedRadioButton);
      this.xAxisSecondaryMinimumPanel.Controls.Add(this.xAxisSecondaryMinimumAutoRadioButton);
      this.xAxisSecondaryMinimumPanel.Location = new System.Drawing.Point(63, 47);
      this.xAxisSecondaryMinimumPanel.Name = "xAxisSecondaryMinimumPanel";
      this.xAxisSecondaryMinimumPanel.Size = new System.Drawing.Size(245, 25);
      this.xAxisSecondaryMinimumPanel.TabIndex = 3;
      // 
      // xAxisSecondaryMinimumFixedTextBox
      // 
      this.xAxisSecondaryMinimumFixedTextBox.Location = new System.Drawing.Point(126, 2);
      this.xAxisSecondaryMinimumFixedTextBox.Name = "xAxisSecondaryMinimumFixedTextBox";
      this.xAxisSecondaryMinimumFixedTextBox.Size = new System.Drawing.Size(95, 20);
      this.xAxisSecondaryMinimumFixedTextBox.TabIndex = 2;
      this.xAxisSecondaryMinimumFixedTextBox.Validating += new System.ComponentModel.CancelEventHandler(this.xAxisSecondaryMinimumFixedTextBox_Validating);
      // 
      // xAxisSecondaryMinimumFixedRadioButton
      // 
      this.xAxisSecondaryMinimumFixedRadioButton.AutoSize = true;
      this.xAxisSecondaryMinimumFixedRadioButton.Location = new System.Drawing.Point(70, 3);
      this.xAxisSecondaryMinimumFixedRadioButton.Name = "xAxisSecondaryMinimumFixedRadioButton";
      this.xAxisSecondaryMinimumFixedRadioButton.Size = new System.Drawing.Size(50, 17);
      this.xAxisSecondaryMinimumFixedRadioButton.TabIndex = 1;
      this.xAxisSecondaryMinimumFixedRadioButton.TabStop = true;
      this.xAxisSecondaryMinimumFixedRadioButton.Text = "Fixed";
      this.xAxisSecondaryMinimumFixedRadioButton.UseVisualStyleBackColor = true;
      this.xAxisSecondaryMinimumFixedRadioButton.CheckedChanged += new System.EventHandler(this.xAxisSecondaryMinimumRadioButton_CheckedChanged);
      // 
      // xAxisSecondaryMinimumAutoRadioButton
      // 
      this.xAxisSecondaryMinimumAutoRadioButton.AutoSize = true;
      this.xAxisSecondaryMinimumAutoRadioButton.Location = new System.Drawing.Point(4, 3);
      this.xAxisSecondaryMinimumAutoRadioButton.Name = "xAxisSecondaryMinimumAutoRadioButton";
      this.xAxisSecondaryMinimumAutoRadioButton.Size = new System.Drawing.Size(47, 17);
      this.xAxisSecondaryMinimumAutoRadioButton.TabIndex = 0;
      this.xAxisSecondaryMinimumAutoRadioButton.TabStop = true;
      this.xAxisSecondaryMinimumAutoRadioButton.Text = "Auto";
      this.xAxisSecondaryMinimumAutoRadioButton.UseVisualStyleBackColor = true;
      this.xAxisSecondaryMinimumAutoRadioButton.CheckedChanged += new System.EventHandler(this.xAxisSecondaryMinimumRadioButton_CheckedChanged);
      // 
      // axisTabControl
      // 
      this.axisTabControl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                  | System.Windows.Forms.AnchorStyles.Left)
                  | System.Windows.Forms.AnchorStyles.Right)));
      this.axisTabControl.Controls.Add(this.mainTabPage);
      this.axisTabControl.Controls.Add(this.xAxisTabPage);
      this.axisTabControl.Controls.Add(this.yAxisTabPage);
      this.axisTabControl.Location = new System.Drawing.Point(0, 0);
      this.axisTabControl.Name = "axisTabControl";
      this.axisTabControl.SelectedIndex = 0;
      this.axisTabControl.Size = new System.Drawing.Size(336, 257);
      this.axisTabControl.TabIndex = 0;
      // 
      // mainTabPage
      // 
      this.mainTabPage.Controls.Add(this.axisFontLabel);
      this.mainTabPage.Controls.Add(this.titleFontLabel);
      this.mainTabPage.Controls.Add(this.axisFontButton);
      this.mainTabPage.Controls.Add(this.label3);
      this.mainTabPage.Controls.Add(this.titleFontButton);
      this.mainTabPage.Controls.Add(this.label2);
      this.mainTabPage.Location = new System.Drawing.Point(4, 22);
      this.mainTabPage.Name = "mainTabPage";
      this.mainTabPage.Size = new System.Drawing.Size(328, 231);
      this.mainTabPage.TabIndex = 2;
      this.mainTabPage.Text = "Main";
      this.mainTabPage.UseVisualStyleBackColor = true;
      // 
      // axisFontLabel
      // 
      this.axisFontLabel.AutoSize = true;
      this.axisFontLabel.Location = new System.Drawing.Point(101, 44);
      this.axisFontLabel.Name = "axisFontLabel";
      this.axisFontLabel.Size = new System.Drawing.Size(13, 13);
      this.axisFontLabel.TabIndex = 2;
      this.axisFontLabel.Text = "()";
      // 
      // titleFontLabel
      // 
      this.titleFontLabel.AutoSize = true;
      this.titleFontLabel.Location = new System.Drawing.Point(101, 15);
      this.titleFontLabel.Name = "titleFontLabel";
      this.titleFontLabel.Size = new System.Drawing.Size(13, 13);
      this.titleFontLabel.TabIndex = 2;
      this.titleFontLabel.Text = "()";
      // 
      // axisFontButton
      // 
      this.axisFontButton.Location = new System.Drawing.Point(69, 39);
      this.axisFontButton.Name = "axisFontButton";
      this.axisFontButton.Size = new System.Drawing.Size(26, 23);
      this.axisFontButton.TabIndex = 1;
      this.axisFontButton.Text = "...";
      this.axisFontButton.UseVisualStyleBackColor = true;
      this.axisFontButton.Click += new System.EventHandler(this.axisFontButton_Click);
      // 
      // label3
      // 
      this.label3.AutoSize = true;
      this.label3.Location = new System.Drawing.Point(9, 44);
      this.label3.Name = "label3";
      this.label3.Size = new System.Drawing.Size(53, 13);
      this.label3.TabIndex = 0;
      this.label3.Text = "Axis Font:";
      // 
      // titleFontButton
      // 
      this.titleFontButton.Location = new System.Drawing.Point(69, 10);
      this.titleFontButton.Name = "titleFontButton";
      this.titleFontButton.Size = new System.Drawing.Size(26, 23);
      this.titleFontButton.TabIndex = 1;
      this.titleFontButton.Text = "...";
      this.titleFontButton.UseVisualStyleBackColor = true;
      this.titleFontButton.Click += new System.EventHandler(this.titleFontButton_Click);
      // 
      // label2
      // 
      this.label2.AutoSize = true;
      this.label2.Location = new System.Drawing.Point(9, 15);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(54, 13);
      this.label2.TabIndex = 0;
      this.label2.Text = "Title Font:";
      // 
      // xAxisTabPage
      // 
      this.xAxisTabPage.Controls.Add(this.xAxisSecondaryGroupBox);
      this.xAxisTabPage.Controls.Add(this.xAxisPrimaryGroupBox);
      this.xAxisTabPage.Location = new System.Drawing.Point(4, 22);
      this.xAxisTabPage.Name = "xAxisTabPage";
      this.xAxisTabPage.Padding = new System.Windows.Forms.Padding(3);
      this.xAxisTabPage.Size = new System.Drawing.Size(328, 231);
      this.xAxisTabPage.TabIndex = 0;
      this.xAxisTabPage.Text = "X-Axis";
      this.xAxisTabPage.UseVisualStyleBackColor = true;
      // 
      // yAxisTabPage
      // 
      this.yAxisTabPage.Controls.Add(this.yAxisSecondaryGroupBox);
      this.yAxisTabPage.Controls.Add(this.yAxisPrimaryGroupBox);
      this.yAxisTabPage.Location = new System.Drawing.Point(4, 22);
      this.yAxisTabPage.Name = "yAxisTabPage";
      this.yAxisTabPage.Padding = new System.Windows.Forms.Padding(3);
      this.yAxisTabPage.Size = new System.Drawing.Size(328, 231);
      this.yAxisTabPage.TabIndex = 1;
      this.yAxisTabPage.Text = "Y-Axis";
      this.yAxisTabPage.UseVisualStyleBackColor = true;
      // 
      // yAxisSecondaryGroupBox
      // 
      this.yAxisSecondaryGroupBox.Controls.Add(this.yAxisSecondaryTitleTextBox);
      this.yAxisSecondaryGroupBox.Controls.Add(this.label4);
      this.yAxisSecondaryGroupBox.Controls.Add(this.label13);
      this.yAxisSecondaryGroupBox.Controls.Add(this.label14);
      this.yAxisSecondaryGroupBox.Controls.Add(this.yAxisSecondaryMaximumPanel);
      this.yAxisSecondaryGroupBox.Controls.Add(this.yAxisSecondaryMinimumPanel);
      this.yAxisSecondaryGroupBox.Location = new System.Drawing.Point(6, 119);
      this.yAxisSecondaryGroupBox.Name = "yAxisSecondaryGroupBox";
      this.yAxisSecondaryGroupBox.Size = new System.Drawing.Size(316, 106);
      this.yAxisSecondaryGroupBox.TabIndex = 1;
      this.yAxisSecondaryGroupBox.TabStop = false;
      this.yAxisSecondaryGroupBox.Text = "Secondary Axis";
      // 
      // label13
      // 
      this.label13.AutoSize = true;
      this.label13.Location = new System.Drawing.Point(4, 77);
      this.label13.Name = "label13";
      this.label13.Size = new System.Drawing.Size(54, 13);
      this.label13.TabIndex = 4;
      this.label13.Text = "Maximum:";
      // 
      // label14
      // 
      this.label14.AutoSize = true;
      this.label14.Location = new System.Drawing.Point(4, 53);
      this.label14.Name = "label14";
      this.label14.Size = new System.Drawing.Size(51, 13);
      this.label14.TabIndex = 2;
      this.label14.Text = "Minimum:";
      // 
      // yAxisSecondaryMaximumPanel
      // 
      this.yAxisSecondaryMaximumPanel.Controls.Add(this.yAxisSecondaryMaximumFixedTextBox);
      this.yAxisSecondaryMaximumPanel.Controls.Add(this.yAxisSecondaryMaximumFixedRadioButton);
      this.yAxisSecondaryMaximumPanel.Controls.Add(this.yAxisSecondaryMaximumAutoRadioButton);
      this.yAxisSecondaryMaximumPanel.Location = new System.Drawing.Point(63, 71);
      this.yAxisSecondaryMaximumPanel.Name = "yAxisSecondaryMaximumPanel";
      this.yAxisSecondaryMaximumPanel.Size = new System.Drawing.Size(245, 25);
      this.yAxisSecondaryMaximumPanel.TabIndex = 5;
      // 
      // yAxisSecondaryMaximumFixedTextBox
      // 
      this.yAxisSecondaryMaximumFixedTextBox.Location = new System.Drawing.Point(126, 2);
      this.yAxisSecondaryMaximumFixedTextBox.Name = "yAxisSecondaryMaximumFixedTextBox";
      this.yAxisSecondaryMaximumFixedTextBox.Size = new System.Drawing.Size(95, 20);
      this.yAxisSecondaryMaximumFixedTextBox.TabIndex = 2;
      this.yAxisSecondaryMaximumFixedTextBox.Validating += new System.ComponentModel.CancelEventHandler(this.yAxisSecondaryMaximumFixedTextBox_Validating);
      // 
      // yAxisSecondaryMaximumFixedRadioButton
      // 
      this.yAxisSecondaryMaximumFixedRadioButton.AutoSize = true;
      this.yAxisSecondaryMaximumFixedRadioButton.Location = new System.Drawing.Point(70, 4);
      this.yAxisSecondaryMaximumFixedRadioButton.Name = "yAxisSecondaryMaximumFixedRadioButton";
      this.yAxisSecondaryMaximumFixedRadioButton.Size = new System.Drawing.Size(50, 17);
      this.yAxisSecondaryMaximumFixedRadioButton.TabIndex = 1;
      this.yAxisSecondaryMaximumFixedRadioButton.TabStop = true;
      this.yAxisSecondaryMaximumFixedRadioButton.Text = "Fixed";
      this.yAxisSecondaryMaximumFixedRadioButton.UseVisualStyleBackColor = true;
      this.yAxisSecondaryMaximumFixedRadioButton.CheckedChanged += new System.EventHandler(this.yAxisSecondaryMaximumRadioButton_CheckedChanged);
      // 
      // yAxisSecondaryMaximumAutoRadioButton
      // 
      this.yAxisSecondaryMaximumAutoRadioButton.AutoSize = true;
      this.yAxisSecondaryMaximumAutoRadioButton.Location = new System.Drawing.Point(4, 4);
      this.yAxisSecondaryMaximumAutoRadioButton.Name = "yAxisSecondaryMaximumAutoRadioButton";
      this.yAxisSecondaryMaximumAutoRadioButton.Size = new System.Drawing.Size(47, 17);
      this.yAxisSecondaryMaximumAutoRadioButton.TabIndex = 0;
      this.yAxisSecondaryMaximumAutoRadioButton.TabStop = true;
      this.yAxisSecondaryMaximumAutoRadioButton.Text = "Auto";
      this.yAxisSecondaryMaximumAutoRadioButton.UseVisualStyleBackColor = true;
      this.yAxisSecondaryMaximumAutoRadioButton.CheckedChanged += new System.EventHandler(this.yAxisSecondaryMaximumRadioButton_CheckedChanged);
      // 
      // yAxisSecondaryMinimumPanel
      // 
      this.yAxisSecondaryMinimumPanel.Controls.Add(this.yAxisSecondaryMinimumFixedTextBox);
      this.yAxisSecondaryMinimumPanel.Controls.Add(this.yAxisSecondaryMinimumFixedRadioButton);
      this.yAxisSecondaryMinimumPanel.Controls.Add(this.yAxisSecondaryMinimumAutoRadioButton);
      this.yAxisSecondaryMinimumPanel.Location = new System.Drawing.Point(63, 47);
      this.yAxisSecondaryMinimumPanel.Name = "yAxisSecondaryMinimumPanel";
      this.yAxisSecondaryMinimumPanel.Size = new System.Drawing.Size(245, 25);
      this.yAxisSecondaryMinimumPanel.TabIndex = 3;
      // 
      // yAxisSecondaryMinimumFixedTextBox
      // 
      this.yAxisSecondaryMinimumFixedTextBox.Location = new System.Drawing.Point(126, 2);
      this.yAxisSecondaryMinimumFixedTextBox.Name = "yAxisSecondaryMinimumFixedTextBox";
      this.yAxisSecondaryMinimumFixedTextBox.Size = new System.Drawing.Size(95, 20);
      this.yAxisSecondaryMinimumFixedTextBox.TabIndex = 2;
      this.yAxisSecondaryMinimumFixedTextBox.Validating += new System.ComponentModel.CancelEventHandler(this.yAxisSecondaryMinimumFixedTextBox_Validating);
      // 
      // yAxisSecondaryMinimumFixedRadioButton
      // 
      this.yAxisSecondaryMinimumFixedRadioButton.AutoSize = true;
      this.yAxisSecondaryMinimumFixedRadioButton.Location = new System.Drawing.Point(70, 3);
      this.yAxisSecondaryMinimumFixedRadioButton.Name = "yAxisSecondaryMinimumFixedRadioButton";
      this.yAxisSecondaryMinimumFixedRadioButton.Size = new System.Drawing.Size(50, 17);
      this.yAxisSecondaryMinimumFixedRadioButton.TabIndex = 1;
      this.yAxisSecondaryMinimumFixedRadioButton.TabStop = true;
      this.yAxisSecondaryMinimumFixedRadioButton.Text = "Fixed";
      this.yAxisSecondaryMinimumFixedRadioButton.UseVisualStyleBackColor = true;
      this.yAxisSecondaryMinimumFixedRadioButton.CheckedChanged += new System.EventHandler(this.yAxisSecondaryMinimumRadioButton_CheckedChanged);
      // 
      // yAxisSecondaryMinimumAutoRadioButton
      // 
      this.yAxisSecondaryMinimumAutoRadioButton.AutoSize = true;
      this.yAxisSecondaryMinimumAutoRadioButton.Location = new System.Drawing.Point(4, 3);
      this.yAxisSecondaryMinimumAutoRadioButton.Name = "yAxisSecondaryMinimumAutoRadioButton";
      this.yAxisSecondaryMinimumAutoRadioButton.Size = new System.Drawing.Size(47, 17);
      this.yAxisSecondaryMinimumAutoRadioButton.TabIndex = 0;
      this.yAxisSecondaryMinimumAutoRadioButton.TabStop = true;
      this.yAxisSecondaryMinimumAutoRadioButton.Text = "Auto";
      this.yAxisSecondaryMinimumAutoRadioButton.UseVisualStyleBackColor = true;
      this.yAxisSecondaryMinimumAutoRadioButton.CheckedChanged += new System.EventHandler(this.yAxisSecondaryMinimumRadioButton_CheckedChanged);
      // 
      // yAxisPrimaryGroupBox
      // 
      this.yAxisPrimaryGroupBox.Controls.Add(this.label7);
      this.yAxisPrimaryGroupBox.Controls.Add(this.yAxisPrimaryTitleTextBox);
      this.yAxisPrimaryGroupBox.Controls.Add(this.label8);
      this.yAxisPrimaryGroupBox.Controls.Add(this.label1);
      this.yAxisPrimaryGroupBox.Controls.Add(this.yAxisPrimaryMaximumPanel);
      this.yAxisPrimaryGroupBox.Controls.Add(this.yAxisPrimaryMinimumPanel);
      this.yAxisPrimaryGroupBox.Location = new System.Drawing.Point(6, 6);
      this.yAxisPrimaryGroupBox.Name = "yAxisPrimaryGroupBox";
      this.yAxisPrimaryGroupBox.Size = new System.Drawing.Size(316, 107);
      this.yAxisPrimaryGroupBox.TabIndex = 0;
      this.yAxisPrimaryGroupBox.TabStop = false;
      this.yAxisPrimaryGroupBox.Text = "Primary Axis";
      // 
      // label7
      // 
      this.label7.AutoSize = true;
      this.label7.Location = new System.Drawing.Point(4, 77);
      this.label7.Name = "label7";
      this.label7.Size = new System.Drawing.Size(54, 13);
      this.label7.TabIndex = 4;
      this.label7.Text = "Maximum:";
      // 
      // label8
      // 
      this.label8.AutoSize = true;
      this.label8.Location = new System.Drawing.Point(4, 53);
      this.label8.Name = "label8";
      this.label8.Size = new System.Drawing.Size(51, 13);
      this.label8.TabIndex = 2;
      this.label8.Text = "Minimum:";
      // 
      // yAxisPrimaryMaximumPanel
      // 
      this.yAxisPrimaryMaximumPanel.Controls.Add(this.yAxisPrimaryMaximumFixedTextBox);
      this.yAxisPrimaryMaximumPanel.Controls.Add(this.yAxisPrimaryMaximumFixedRadioButton);
      this.yAxisPrimaryMaximumPanel.Controls.Add(this.yAxisPrimaryMaximumAutoRadioButton);
      this.yAxisPrimaryMaximumPanel.Location = new System.Drawing.Point(63, 71);
      this.yAxisPrimaryMaximumPanel.Name = "yAxisPrimaryMaximumPanel";
      this.yAxisPrimaryMaximumPanel.Size = new System.Drawing.Size(245, 25);
      this.yAxisPrimaryMaximumPanel.TabIndex = 5;
      // 
      // yAxisPrimaryMaximumFixedTextBox
      // 
      this.yAxisPrimaryMaximumFixedTextBox.Location = new System.Drawing.Point(126, 2);
      this.yAxisPrimaryMaximumFixedTextBox.Name = "yAxisPrimaryMaximumFixedTextBox";
      this.yAxisPrimaryMaximumFixedTextBox.Size = new System.Drawing.Size(95, 20);
      this.yAxisPrimaryMaximumFixedTextBox.TabIndex = 2;
      this.yAxisPrimaryMaximumFixedTextBox.Validating += new System.ComponentModel.CancelEventHandler(this.yAxisPrimaryMaximumFixedTextBox_Validating);
      // 
      // yAxisPrimaryMaximumFixedRadioButton
      // 
      this.yAxisPrimaryMaximumFixedRadioButton.AutoSize = true;
      this.yAxisPrimaryMaximumFixedRadioButton.Location = new System.Drawing.Point(70, 4);
      this.yAxisPrimaryMaximumFixedRadioButton.Name = "yAxisPrimaryMaximumFixedRadioButton";
      this.yAxisPrimaryMaximumFixedRadioButton.Size = new System.Drawing.Size(50, 17);
      this.yAxisPrimaryMaximumFixedRadioButton.TabIndex = 1;
      this.yAxisPrimaryMaximumFixedRadioButton.TabStop = true;
      this.yAxisPrimaryMaximumFixedRadioButton.Text = "Fixed";
      this.yAxisPrimaryMaximumFixedRadioButton.UseVisualStyleBackColor = true;
      this.yAxisPrimaryMaximumFixedRadioButton.CheckedChanged += new System.EventHandler(this.yAxisPrimaryMaximumRadioButton_CheckedChanged);
      // 
      // yAxisPrimaryMaximumAutoRadioButton
      // 
      this.yAxisPrimaryMaximumAutoRadioButton.AutoSize = true;
      this.yAxisPrimaryMaximumAutoRadioButton.Location = new System.Drawing.Point(4, 4);
      this.yAxisPrimaryMaximumAutoRadioButton.Name = "yAxisPrimaryMaximumAutoRadioButton";
      this.yAxisPrimaryMaximumAutoRadioButton.Size = new System.Drawing.Size(47, 17);
      this.yAxisPrimaryMaximumAutoRadioButton.TabIndex = 0;
      this.yAxisPrimaryMaximumAutoRadioButton.TabStop = true;
      this.yAxisPrimaryMaximumAutoRadioButton.Text = "Auto";
      this.yAxisPrimaryMaximumAutoRadioButton.UseVisualStyleBackColor = true;
      this.yAxisPrimaryMaximumAutoRadioButton.CheckedChanged += new System.EventHandler(this.yAxisPrimaryMaximumRadioButton_CheckedChanged);
      // 
      // yAxisPrimaryMinimumPanel
      // 
      this.yAxisPrimaryMinimumPanel.Controls.Add(this.yAxisPrimaryMinimumFixedTextBox);
      this.yAxisPrimaryMinimumPanel.Controls.Add(this.yAxisPrimaryMinimumFixedRadioButton);
      this.yAxisPrimaryMinimumPanel.Controls.Add(this.yAxisPrimaryMinimumAutoRadioButton);
      this.yAxisPrimaryMinimumPanel.Location = new System.Drawing.Point(63, 47);
      this.yAxisPrimaryMinimumPanel.Name = "yAxisPrimaryMinimumPanel";
      this.yAxisPrimaryMinimumPanel.Size = new System.Drawing.Size(245, 25);
      this.yAxisPrimaryMinimumPanel.TabIndex = 3;
      // 
      // yAxisPrimaryMinimumFixedTextBox
      // 
      this.yAxisPrimaryMinimumFixedTextBox.Location = new System.Drawing.Point(126, 2);
      this.yAxisPrimaryMinimumFixedTextBox.Name = "yAxisPrimaryMinimumFixedTextBox";
      this.yAxisPrimaryMinimumFixedTextBox.Size = new System.Drawing.Size(95, 20);
      this.yAxisPrimaryMinimumFixedTextBox.TabIndex = 2;
      this.yAxisPrimaryMinimumFixedTextBox.Validating += new System.ComponentModel.CancelEventHandler(this.yAxisPrimaryMinimumFixedTextBox_Validating);
      // 
      // yAxisPrimaryMinimumFixedRadioButton
      // 
      this.yAxisPrimaryMinimumFixedRadioButton.AutoSize = true;
      this.yAxisPrimaryMinimumFixedRadioButton.Location = new System.Drawing.Point(70, 3);
      this.yAxisPrimaryMinimumFixedRadioButton.Name = "yAxisPrimaryMinimumFixedRadioButton";
      this.yAxisPrimaryMinimumFixedRadioButton.Size = new System.Drawing.Size(50, 17);
      this.yAxisPrimaryMinimumFixedRadioButton.TabIndex = 1;
      this.yAxisPrimaryMinimumFixedRadioButton.TabStop = true;
      this.yAxisPrimaryMinimumFixedRadioButton.Text = "Fixed";
      this.yAxisPrimaryMinimumFixedRadioButton.UseVisualStyleBackColor = true;
      this.yAxisPrimaryMinimumFixedRadioButton.CheckedChanged += new System.EventHandler(this.yAxisPrimaryMinimumRadioButton_CheckedChanged);
      // 
      // yAxisPrimaryMinimumAutoRadioButton
      // 
      this.yAxisPrimaryMinimumAutoRadioButton.AutoSize = true;
      this.yAxisPrimaryMinimumAutoRadioButton.Location = new System.Drawing.Point(4, 3);
      this.yAxisPrimaryMinimumAutoRadioButton.Name = "yAxisPrimaryMinimumAutoRadioButton";
      this.yAxisPrimaryMinimumAutoRadioButton.Size = new System.Drawing.Size(47, 17);
      this.yAxisPrimaryMinimumAutoRadioButton.TabIndex = 0;
      this.yAxisPrimaryMinimumAutoRadioButton.TabStop = true;
      this.yAxisPrimaryMinimumAutoRadioButton.Text = "Auto";
      this.yAxisPrimaryMinimumAutoRadioButton.UseVisualStyleBackColor = true;
      this.yAxisPrimaryMinimumAutoRadioButton.CheckedChanged += new System.EventHandler(this.yAxisPrimaryMinimumRadioButton_CheckedChanged);
      // 
      // errorProvider
      // 
      this.errorProvider.ContainerControl = this;
      // 
      // titleFontDialog
      // 
      this.titleFontDialog.FontMustExist = true;
      this.titleFontDialog.ShowColor = true;
      // 
      // DataTableVisualPropertiesControl
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this.axisTabControl);
      this.Name = "DataTableVisualPropertiesControl";
      this.Size = new System.Drawing.Size(336, 257);
      this.xAxisPrimaryGroupBox.ResumeLayout(false);
      this.xAxisPrimaryGroupBox.PerformLayout();
      this.xAxisPrimaryMaximumPanel.ResumeLayout(false);
      this.xAxisPrimaryMaximumPanel.PerformLayout();
      this.xAxisPrimaryMinimumPanel.ResumeLayout(false);
      this.xAxisPrimaryMinimumPanel.PerformLayout();
      this.xAxisSecondaryGroupBox.ResumeLayout(false);
      this.xAxisSecondaryGroupBox.PerformLayout();
      this.xAxisSecondaryMaximumPanel.ResumeLayout(false);
      this.xAxisSecondaryMaximumPanel.PerformLayout();
      this.xAxisSecondaryMinimumPanel.ResumeLayout(false);
      this.xAxisSecondaryMinimumPanel.PerformLayout();
      this.axisTabControl.ResumeLayout(false);
      this.mainTabPage.ResumeLayout(false);
      this.mainTabPage.PerformLayout();
      this.xAxisTabPage.ResumeLayout(false);
      this.yAxisTabPage.ResumeLayout(false);
      this.yAxisSecondaryGroupBox.ResumeLayout(false);
      this.yAxisSecondaryGroupBox.PerformLayout();
      this.yAxisSecondaryMaximumPanel.ResumeLayout(false);
      this.yAxisSecondaryMaximumPanel.PerformLayout();
      this.yAxisSecondaryMinimumPanel.ResumeLayout(false);
      this.yAxisSecondaryMinimumPanel.PerformLayout();
      this.yAxisPrimaryGroupBox.ResumeLayout(false);
      this.yAxisPrimaryGroupBox.PerformLayout();
      this.yAxisPrimaryMaximumPanel.ResumeLayout(false);
      this.yAxisPrimaryMaximumPanel.PerformLayout();
      this.yAxisPrimaryMinimumPanel.ResumeLayout(false);
      this.yAxisPrimaryMinimumPanel.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.TextBox yAxisPrimaryTitleTextBox;
    private System.Windows.Forms.TextBox yAxisSecondaryTitleTextBox;
    private System.Windows.Forms.Label label4;
    private System.Windows.Forms.GroupBox xAxisPrimaryGroupBox;
    private System.Windows.Forms.Label label12;
    private System.Windows.Forms.Label label9;
    private System.Windows.Forms.TextBox xAxisPrimaryTitleTextBox;
    private System.Windows.Forms.Label label11;
    private System.Windows.Forms.Panel xAxisPrimaryMaximumPanel;
    private System.Windows.Forms.TextBox xAxisPrimaryMaximumFixedTextBox;
    private System.Windows.Forms.RadioButton xAxisPrimaryMaximumFixedRadioButton;
    private System.Windows.Forms.RadioButton xAxisPrimaryMaximumAutoRadioButton;
    private System.Windows.Forms.Panel xAxisPrimaryMinimumPanel;
    private System.Windows.Forms.TextBox xAxisPrimaryMinimumFixedTextBox;
    private System.Windows.Forms.RadioButton xAxisPrimaryMinimumFixedRadioButton;
    private System.Windows.Forms.RadioButton xAxisPrimaryMinimumAutoRadioButton;
    private System.Windows.Forms.GroupBox xAxisSecondaryGroupBox;
    private System.Windows.Forms.Label label10;
    private System.Windows.Forms.TextBox xAxisSecondaryTitleTextBox;
    private System.Windows.Forms.Label label5;
    private System.Windows.Forms.Label label6;
    private System.Windows.Forms.Panel xAxisSecondaryMaximumPanel;
    private System.Windows.Forms.TextBox xAxisSecondaryMaximumFixedTextBox;
    private System.Windows.Forms.RadioButton xAxisSecondaryMaximumFixedRadioButton;
    private System.Windows.Forms.RadioButton xAxisSecondaryMaximumAutoRadioButton;
    private System.Windows.Forms.Panel xAxisSecondaryMinimumPanel;
    private System.Windows.Forms.TextBox xAxisSecondaryMinimumFixedTextBox;
    private System.Windows.Forms.RadioButton xAxisSecondaryMinimumFixedRadioButton;
    private System.Windows.Forms.RadioButton xAxisSecondaryMinimumAutoRadioButton;
    private System.Windows.Forms.TabControl axisTabControl;
    private System.Windows.Forms.TabPage xAxisTabPage;
    private System.Windows.Forms.TabPage yAxisTabPage;
    private System.Windows.Forms.GroupBox yAxisSecondaryGroupBox;
    private System.Windows.Forms.Label label13;
    private System.Windows.Forms.Label label14;
    private System.Windows.Forms.Panel yAxisSecondaryMaximumPanel;
    private System.Windows.Forms.TextBox yAxisSecondaryMaximumFixedTextBox;
    private System.Windows.Forms.RadioButton yAxisSecondaryMaximumFixedRadioButton;
    private System.Windows.Forms.RadioButton yAxisSecondaryMaximumAutoRadioButton;
    private System.Windows.Forms.Panel yAxisSecondaryMinimumPanel;
    private System.Windows.Forms.TextBox yAxisSecondaryMinimumFixedTextBox;
    private System.Windows.Forms.RadioButton yAxisSecondaryMinimumFixedRadioButton;
    private System.Windows.Forms.RadioButton yAxisSecondaryMinimumAutoRadioButton;
    private System.Windows.Forms.GroupBox yAxisPrimaryGroupBox;
    private System.Windows.Forms.Label label7;
    private System.Windows.Forms.Label label8;
    private System.Windows.Forms.Panel yAxisPrimaryMaximumPanel;
    private System.Windows.Forms.TextBox yAxisPrimaryMaximumFixedTextBox;
    private System.Windows.Forms.RadioButton yAxisPrimaryMaximumFixedRadioButton;
    private System.Windows.Forms.RadioButton yAxisPrimaryMaximumAutoRadioButton;
    private System.Windows.Forms.Panel yAxisPrimaryMinimumPanel;
    private System.Windows.Forms.TextBox yAxisPrimaryMinimumFixedTextBox;
    private System.Windows.Forms.RadioButton yAxisPrimaryMinimumFixedRadioButton;
    private System.Windows.Forms.RadioButton yAxisPrimaryMinimumAutoRadioButton;
    private System.Windows.Forms.ErrorProvider errorProvider;
    private System.Windows.Forms.TabPage mainTabPage;
    private System.Windows.Forms.FontDialog titleFontDialog;
    private System.Windows.Forms.Button titleFontButton;
    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.Button axisFontButton;
    private System.Windows.Forms.Label label3;
    private System.Windows.Forms.Label axisFontLabel;
    private System.Windows.Forms.Label titleFontLabel;
    private System.Windows.Forms.FontDialog axisFontDialog;
  }
}
