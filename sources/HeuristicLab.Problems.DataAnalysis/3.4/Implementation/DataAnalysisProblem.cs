#region License Information
/* HeuristicLab
 * Copyright (C) 2002-2011 Heuristic and Evolutionary Algorithms Laboratory (HEAL)
 *
 * This file is part of HeuristicLab.
 *
 * HeuristicLab is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HeuristicLab is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HeuristicLab. If not, see <http://www.gnu.org/licenses/>.
 */
#endregion

using System;
using HeuristicLab.Common;
using HeuristicLab.Core;
using HeuristicLab.Optimization;
using HeuristicLab.Parameters;
using HeuristicLab.Persistence.Default.CompositeSerializers.Storable;

namespace HeuristicLab.Problems.DataAnalysis {
  [StorableClass]
  public abstract class DataAnalysisProblem<T> : Problem,
    IDataAnalysisProblem<T>
    where T : class, IDataAnalysisProblemData {
    private const string ProblemDataParameterName = "ProblemData";
    private const string ProblemDataParameterDescription = "The data set, target variable and input variables of the data analysis problem.";
    #region parameter properties
    IParameter IDataAnalysisProblem.ProblemDataParameter {
      get { return ProblemDataParameter; }
    }
    public IValueParameter<T> ProblemDataParameter {
      get { return (IValueParameter<T>)Parameters[ProblemDataParameterName]; }
    }
    #endregion
    #region properties
    IDataAnalysisProblemData IDataAnalysisProblem.ProblemData {
      get { return ProblemData; }
    }
    public T ProblemData {
      get { return ProblemDataParameter.Value; }
      protected set { ProblemDataParameter.Value = value; }
    }
    #endregion
    protected DataAnalysisProblem(DataAnalysisProblem<T> original, Cloner cloner)
      : base(original, cloner) {
    }
    [StorableConstructor]
    protected DataAnalysisProblem(bool deserializing) : base(deserializing) { }
    public DataAnalysisProblem()
      : base() {
      Parameters.Add(new ValueParameter<T>(ProblemDataParameterName, ProblemDataParameterDescription));
    }

    private void RegisterEventHandlers() {
      ProblemDataParameter.Value.Changed += new EventHandler(ProblemDataParameter_ValueChanged);
    }
    private void ProblemDataParameter_ValueChanged(object sender, EventArgs e) {
      OnProblemDataChanged();
      OnReset();
    }

    public event EventHandler ProblemDataChanged;
    protected virtual void OnProblemDataChanged() {
      var handler = ProblemDataChanged;
      if (handler != null) handler(this, EventArgs.Empty);
    }

    public abstract void ImportProblemDataFromFile(string fileName);
  }
}
